/**
 *
 */

 $(function() {
	"use strict";

	/*href에 #을 포함(*=)하고 있지만 href가 정확히 #는 아닌 a태그*/
	$("a.scroll-trigger[href*='#']:not([href='#'])").click(function() {
		let hashTarget = $(this.hash);
		if(hashTarget.length) {
			// 해당 타겟으로 스크롤한다.
			$("html, body").animate(
				{ scrollTop: hashTarget.offset().top - 62}
			);

			// 스크롤 이동시 navbar를 접도록
			$(".navbar-collapse").collapse("hide");
			return false;
		}
	});

	let mainNav = $("#mainNav");
	let scrollCallback = function() {
		mainNav.offset().top > 84
			? mainNav.removeClass("navbar-on-top")
			: mainNav.addClass("navbar-on-top");
	};
	scrollCallback(); // 처음에 한번 실행해서 mainNav에 navbar-on-top를 추가/제거한다.
	$(window).scroll(scrollCallback);
});